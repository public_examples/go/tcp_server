package main

import (
        "bufio"
        "fmt"
        "net"
        "strings"
)

func handleConnection(c net.Conn) {
		// We got a connection,  we can both read and write to this connection. 
        fmt.Printf("Serving %s\n", c.RemoteAddr().String()) // log a message, remove this unless you need it,  this creates sync IO for each connection.
        for {
                netData, err := bufio.NewReader(c).ReadString('\n') // Sit and wait for newlines, no timeouts here
                if err != nil {
                        fmt.Println(err)
                        return
                }

                temp := strings.TrimSpace(string(netData)) // cleanup the data we got
                if temp == "STOP" {
                        break // if we see the special message "STOP\n",  exit and disconect
                }

                var result = "TEST\n" // our Default Response,  unhandled cases will get this message
                switch temp {
                        case "OPTION1":
                                result = "RESPONSE_1\n"
                        case "OPTION2":
                                result = "RESPONSE_2\n"
                } // random examples. 

                c.Write([]byte(string(result))) // write the bytes onto the socket
        } // wait for more data
        c.Close() // disconnect
}

func main() {
        l, err := net.Listen("tcp4", ":6123") // Listen on 0.0.0.0:6123 statically
        if err != nil {
                fmt.Println(err) // If we are unable to bind,  emit the error and exit
                return
        }
        defer l.Close() // trigger the close action to cleanup our socket when we exit

        for {
                c, err := l.Accept() // Get a new connection off the stack
                if err != nil {
                        fmt.Println(err)
                        return
                }
                go handleConnection(c) // start a goroutine with the connection
        }
}