This example application demonstrates using Go to setup a SUPER simple TCP server that: 

1. Receives connections
2. Starts a goroutine per connection
3. Waits forever for the client to send a string delineated by a "Newline" (`\n`)
4. Parses and cleans up the input
5. Sets up a message to return to the connection
6. Replaces the return value with other strings, if the input matched specific strings
7. Sends the output back to the client
8. Repeats until it receives the special message `STOP\n`
9. Closes the connection

This was written as a simle example for: 

https://www.reddit.com/r/linuxadmin/comments/17tyopk/netcat_send_response_based_on_message_received/

To get this going: 

1. https://go.dev/doc/install
2. `git clone` this project
3. Assuming your GOBIN is set and in the user's path:  in this project dir: `go install`
4. Execute the binary `tcpserver`

You can then test the application by sending it messages,  using netcat as an example: 

`nc 127.0.0.1 6123`

```
> OPTION1
< RESPONSE_1
> OPTION2
< RESPONSE_2
> TEST
< TEST
> OTHERVALUE
< TEST
> STOP
< 
[Netcat closes]
```
